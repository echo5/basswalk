<?php
/*
Template Name: Header Template
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php echo fusion_body_atts(); ?>>

    <nav class="navbar navbar-custom navbar-toggleable-md">
        <?php
			the_content();
		?>
    </nav>

    <?php
get_footer();