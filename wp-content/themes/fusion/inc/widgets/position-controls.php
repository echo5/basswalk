<?php

/**
 * Position controls
 */

function fusion_add_position_options_to_widget($element, $section_id, $args) {

    if ( $section_id == 'section_advanced' || '_section_style' === $section_id ) {

        $element->start_controls_section(
            '_section_position',
            [
                'label' => esc_html__( 'Position', 'fusion' ),
                'tab' => \Elementor\Controls_Manager::TAB_ADVANCED,
            ]
        );

        // $element->add_control(
        //     'rotate_item',
        //     [
        //         'label' => esc_html__( 'Move Element', 'fusion' ),
        //         'type' => \Elementor\Controls_Manager::SWITCHER,
        //         'default' => '',
        //         'label_on' => esc_html__( 'On', 'fusion' ),
        //         'label_off' => esc_html__( 'Off', 'fusion' ),
        //         'return_value' => 'yes',
        //     ]
        // );

        $element->add_control(
            'element_position',
            [
                'type' => \Elementor\Controls_Manager::SELECT,
                'label' => esc_html__( 'Position', 'fusion' ),
                 'default' => '',
                 'options' => [
                    ''  => esc_html__( 'Default', 'fusion' ),
                    'static'  => esc_html__( 'Static', 'fusion' ),
                    'relative' => esc_html__( 'Relative', 'fusion' ),
                    'absolute' => esc_html__( 'Absolute', 'fusion' ),
                    'fixed' => esc_html__( 'Fixed', 'fusion' ),
                 ],
                 'selectors' => [ // You can use the selected value in an auto-generated css rule.
                    '{{WRAPPER}}' => 'position: {{VALUE}}',
                 ],
            ]
        );

        $element->add_control(
            'element_left',
            [
                'label' => esc_html__( 'Left', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}' => 'left: {{SIZE}}{{UNIT}} !important;',
                ],
                'condition' => [
                    'element_position!' => array('','static'),
                ],
            ]
        );
        $element->add_control(
            'element_top',
            [
                'label' => esc_html__( 'Top', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}' => 'top: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'element_position!' => array('','static'),
                ],
            ]
        );

        $element->add_control(
            'element_rotate',
            [
                'label' => esc_html__( 'Rotate Item', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => esc_html__( 'On', 'fusion' ),
                'label_off' => esc_html__( 'Off', 'fusion' ),
                'return_value' => 'yes',
            ]
        );

        $element->add_control(
            'element_rotate_degrees',
            [
                'label' => esc_html__( 'Rotate', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                ],
                'range' => [
                    'px' => [
                        'min' => -180,
                        'max' => 180,
                    ],
                ],
                'size_units' => [ 'px' ],
                'selectors' => [
                    '{{WRAPPER}}' => 'transform: rotate({{SIZE}}deg);',
                ],
                'condition' => [
                    'element_rotate' => 'yes',
                ],
            ]
        );

        $element->add_control(
            'element_transform_origin',
            [
                'label' => esc_html__( 'Transform Origin', 'fusion' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => '50% 50%',
                'selectors' => [
                    '{{WRAPPER}}' => 'transform-origin: {{VALUE}};',
                ],
                'condition' => [
                    'element_rotate' => 'yes',
                ],
            ]
        );

        // $element->add_control(
        //     'z_index',
        //     [
        //         'label' => esc_html__( 'Z-Index', 'fusion' ),
        //         'type' => \Elementor\Controls_Manager::NUMBER,
        //         'default' => '',
        //         'selectors' => [
        //             '{{WRAPPER}}' => 'z-index: {{VALUE}};',
        //         ],
        //         'condition' => [
        //             'element_position!' => array('','static'),
        //         ],
        //     ]
        // );

        $element->add_control(
            'overflow',
            [
                'label' => esc_html__( 'Overflow', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                   ''  => esc_html__( 'Default', 'fusion' ),
                   'hidden' => esc_html__( 'Hidden', 'fusion' ),
                   'visible' => esc_html__( 'Visible', 'fusion' ),
                ],
                'selectors' => [
                    '{{WRAPPER}}' => 'overflow: {{VALUE}};',
                ],
            ]
        );

        $element->end_controls_section();

    }

}
add_action( 'elementor/element/after_section_end', 'fusion_add_position_options_to_widget', 10, 3);
