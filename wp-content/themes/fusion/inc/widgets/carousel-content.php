<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Content_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'content-carousel';
	}

	public function get_title() {
		return esc_html__( 'Content Carousel', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content_carousel',
			[
				'label' => esc_html__( 'Carousel', 'fusion' ),
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => esc_html__( 'Slides', 'fusion' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'slide_content' => esc_html__( 'Slide #1 content', 'fusion' ),
					],
					[
						'slide_content' => esc_html__( 'Slide #2 content', 'fusion' ),
					],
				],
				'fields' => [
					[
						'name' => 'slide_content',
						'label' => esc_html__( 'Content', 'fusion' ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => esc_html__( 'Slide Content', 'fusion' ),
						'show_label' => false,
					],
				],
				// 'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->carousel_controls();

	}

	protected function render() {

		$settings = $this->get_settings();
		$slides = [];
		$content_slides = $settings['slides'];

		if (empty($content_slides))
			return;

		foreach ($content_slides as $slide) {
			$slides[] = $slide['slide_content'];
		}

		$this->render_carousel($slides, $settings);

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Content_Carousel() );