<?php

LuxeOption::add_section( 'maps', array(
    'title'          => esc_attr__( 'Maps', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Maps
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'google_maps_api_key',
    'label'       => esc_attr__( 'Google Maps API Key', 'fusion' ),
    'description' => esc_attr__( 'Enter your Google maps API key here. https://developers.google.com/maps/documentation/javascript/get-api-key', 'fusion' ),
    'section'     => 'maps',
    'default'     => '',
    'priority'    => 10,
) );
