<?php

LuxeOption::add_section( 'nav_light', array(
    'title'          => esc_attr__( 'Light Background Navigation', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'nav'
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'nav_typography_light_color',
    'label'       => esc_attr__( 'Light Navigation Font Color', 'fusion' ),
    'section'     => 'nav_light',
    'default'     => '#212121',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar-nav .nav-link',
            'property' => 'color',
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'nav_typography_light_color_hover',
    'label'       => esc_attr__( 'Light Navigation Font Hover Color', 'fusion' ),
    'section'     => 'nav_light',
    'default'     => '#515151',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar-nav .nav-link:hover, .header-light-active .navbar-nav .current-menu-item .nav-link',
            'property' => 'color',
        ),
    ),
) );