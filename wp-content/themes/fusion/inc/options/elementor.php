<?php

LuxeOption::add_section( 'elementor', array(
    'title'          => esc_attr__( 'Elementor', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * General elementor settings
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'elementor_detached',
    'label'       => esc_attr__( 'Detach Editor', 'fusion' ),
    'description' => esc_attr__( 'Detaching the editor will allow you to freely move the Elementor around when editing a page.', 'fusion' ),
    'section'     => 'elementor',
    'default'     => true,
    'priority'    => 10,
) );