<?php

LuxeOption::add_section( 'portfolio', array(
    'title'          => esc_attr__( 'Portfolio', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
LuxeOption::add_section( 'posts', array(
    'title'          => esc_attr__( 'Blog Posts', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Portfolio
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'portfolio_slug',
    'label'       => esc_attr__( 'Portfolio URL Slug', 'fusion' ),
    // 'help'        => esc_attr__( 'This is a tooltip', 'fusion' ),
    'description' => esc_attr__( 'The slug used in your portfolio URL for items.  http://yourdomain.com/<span style="color:red;"">portfolio-item</span>/your-item-name', 'fusion' ),
    'default'     => 'portfolio-item',
    'section'     => 'portfolio',
    'default'     => '',
    'priority'    => 10,
) );
$portfolio_single_styles = apply_filters( 'luxe_portfolio_single_styles', array('default' => 'Default'));
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'portfolio_single_style',
    'label'       => esc_attr__( 'Single Portfolio Item Style', 'fusion' ),
    'description' => esc_attr__( 'Pick the style and layout of your individual portfolio items.', 'fusion' ),
    'section'     => 'portfolio',
    'default'     => 'default',
    'priority'    => 10,
    'choices'     => $portfolio_single_styles
) );

/**
 * Posts
 */
$post_single_styles = apply_filters( 'luxe_post_single_styles', array('default' => 'Default'));
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'post_single_style',
    'label'       => esc_attr__( 'Single Post Style', 'fusion' ),
    'description' => esc_attr__( 'Pick the style and layout of your individual blog posts.', 'fusion' ),
    'section'     => 'posts',
    'default'     => 'default',
    'priority'    => 10,
    'choices'     => $post_single_styles
) );
$post_grid_styles = apply_filters( 'luxe_post_grid_styles', array('default' => 'Default'));
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'post_grid_style',
    'label'       => esc_attr__( 'Post Index Style', 'fusion' ),
    'description' => esc_attr__( 'Pick the style and layout of your blog page.', 'fusion' ),
    'section'     => 'posts',
    'default'     => 'default',
    'priority'    => 10,
    'choices'     => $post_grid_styles
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'blog_header_scheme',
    'label'       => esc_attr__( 'Blog, Archive, and Single Header Scheme', 'fusion' ),
    'description' => esc_attr__( 'Select default header used for blog pages and archive pages.', 'fusion' ),
    'section'     => 'posts',
    'default'     => 'dark',
    'priority'    => 10,
    'choices'     => array(
        'dark' => esc_attr__( 'Dark Background Header', 'fusion' ),
        'light'   => esc_attr__( 'Light Background Header', 'fusion' ),
    ),
) );

