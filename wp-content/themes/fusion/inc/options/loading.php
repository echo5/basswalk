<?php

LuxeOption::add_section( 'loading', array(
    'title'          => esc_attr__( 'Loading', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * General page loading
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'ajax',
    'label'       => esc_attr__( 'AJAX Loading', 'fusion' ),
    'description' => esc_attr__( 'Attempts to load every page through AJAX when links are clicked.  This feature may not work with third party plugins using Javascript.', 'fusion' ),
    'section'     => 'loading',
    'default'     => false,
    'priority'    => 10,
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'ajax_transition',
    'label'       => esc_attr__( 'AJAX Transition', 'fusion' ),
    'description' => esc_attr__( 'The animation effect used when a new page is loaded via AJAX.', 'fusion' ),
    'section'     => 'loading',
    'default'     => 'FadeTransition',
    'priority'    => 10,
    'choices'     => array(
        'FadeTransition'   => esc_attr__( 'Fade', 'fusion' ),
        'WipeLeftTransition' => esc_attr__( 'Wipe Left', 'fusion' ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'loading_icon_color',
    'label'       => esc_attr__( 'Loading Icon Color', 'fusion' ),
    'description' => esc_attr__( 'The color of the loading icon and progress bar.', 'fusion' ),
    'section'     => 'loading',
    'default'     => '#2ca3d4',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.pace .pace-progress',
            'property' => 'background-color',
        ),
        array(
            'element'  => '.pace .pace-activity',
            'property' => 'border-top-color',
        ),
        array(
            'element'  => '.pace .pace-activity',
            'property' => 'border-left-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.pace .pace-progress',
            'function' => 'css',
            'property' => 'background-color',
        ),
        array(
            'element'  => '.pace .pace-activity',
            'function' => 'css',
            'property' => 'border-top-color',
        ),
        array(
            'element'  => '.pace .pace-activity',
            'function' => 'css',
            'property' => 'border-left-color',
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'loading_bg_color',
    'label'       => esc_attr__( 'Loading Background Color', 'fusion' ),
    'description' => esc_attr__( 'The background color of the overlay between page loads.', 'fusion' ),
    'section'     => 'loading',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.loading-screen',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.loading-screen',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
