(function($) {
    'use strict';

    
    $(document).ready(function(){
        
        var body = $('body');
        body.addClass('movable-editor');
        $( "#elementor-panel" ).wrap('<div id="elementor-panel-wrapper"></div>')

        $( "#elementor-panel-wrapper" ).draggable({
          cancel: ".elementor-element-wrapper, input, .elementor-control-input-wrapper, textarea, select" ,
          start: function() {
            
          },
          drag: function(e) {
            body.addClass('dragging-panel');
          },
          stop: function() {
            body.removeClass('dragging-panel');
          }
        });
        $( "#elementor-panel-wrapper").resizable({
            start: function( event, ui ) {
                body.addClass('dragging-panel');
            },
            stop: function( event, ui ) {
                body.removeClass('dragging-panel');
            }
        });

    });

    jQuery(window).on('elementor/frontend/init', function () {
        elementorFrontend.hooks.addAction('frontend/element_ready/widget', function(){
            // initGrids();
            // initGridHover();
            // materializeForms();
            // initParallaxLayers();
            // startAnimations();
            // initSticky();
            // initParticles();
            // initAjaxComments();
            // initProductSingle();
            // initMaps();
            // objectFitImages();
        });
    });

})(jQuery);