<div class="testimonial text-<?php echo $alignment; ?>">
	<div class="testimonial-content h3 mb-3">
		<?php echo $content; ?>
	</div>
	<?php if (!empty($user_image['url'])): ?>
		<div class="testimonial-user-image mb-3">
			<img src="<?php echo $user_image['url']; ?>" alt="<?php echo $user_name; ?>">
		</div>
	<?php endif; ?>
	<div class="testimonial-user h4">
		<?php echo $user_name; ?>
	</div>
	<div class="testimonial-user-title h5">
		<?php echo $user_title; ?>
	</div>
</div>
