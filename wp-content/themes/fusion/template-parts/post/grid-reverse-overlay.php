<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->

	<div class="overlay">
			<div class="categories mb-2">
				<?php the_category(', '); ?>
			</div>
			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
			<?php the_excerpt(); ?>
			<div class="post-meta">
				<span class="author"><?php the_author(); ?></span>
				<span class="date"><?php the_date(fusion_default_date_format()); ?></span>
			</div>
			<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div>

</article><!-- #post-## -->