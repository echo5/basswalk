<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<div class="container">
			<header class="entry-header page-header">
				<div class="row">
					<div class="col-md-6">
						<?php the_title( '<h1 class="entry-title mb-5">', '</h1>' ); ?>
					</div>
					<div class="col-md-6">
						<?php get_template_part('template-parts/portfolio/details'); ?>
					</div>
				</div>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					the_content();
				?>
			</div><!-- .entry-content -->

			<footer class="fixed">
				<?php the_post_navigation(); ?>
			</footer>

		</div>
	
</article><!-- #post-## -->