<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->
	
	<div class="overlay">
		<div class="container">
			<div class="categories mb-3">
				<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
			</div>
			<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
		</div>

		<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div>

</article><!-- #post-## -->