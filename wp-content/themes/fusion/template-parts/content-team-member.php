<div class="team-member team-member-info-<?php echo $info_position; ?>">
	<img src="<?php echo $image; ?>" alt="<?php echo esc_attr($name); ?>">
	<div class="team-member-info">
		<div class="team-member-name h2 mb-2"><?php echo $name; ?></div>
		<div class="team-member-title h5"><?php echo $title; ?></div>
	</div>
</div>