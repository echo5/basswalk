<?php
/**
 * fusion functions and definitions
 *
 * @package fusion
 */

/**
 * Add helper functions
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Setup
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Setup
 */
require get_template_directory() . '/inc/elementor.php';

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * TGM Plugin Activation
 */
require get_template_directory() . '/inc/vendor/class-tgm-plugin-activation.php';

/**
 * Plugin recommendations and installer
 */
require get_template_directory() . '/inc/plugins.php';

/**
 * One click demo installation
 */
require get_template_directory() . '/inc/importer.php';

/**
 * Elementor widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Theme config
 */
require get_template_directory() . '/inc/theme-config.php';