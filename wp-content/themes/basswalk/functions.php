<?php

/**
 * Enqueue child theme styles
 */
function fusion_child_enqueue_styles() {
    wp_enqueue_style('bw-style',
        get_stylesheet_directory_uri() . '/style.css'
	);
	wp_enqueue_style('bw-datepicker',
		get_stylesheet_directory_uri() . '/assets/css/flatpickr.min.css'
	);
	wp_enqueue_script('bw-datepicker',
		get_stylesheet_directory_uri() . '/assets/js/flatpickr.js', array(), false, true
	);
	wp_enqueue_script('bw-main',
		get_stylesheet_directory_uri() . '/assets/js/main.js', array(), false, true
	);
	wp_localize_script( 'bw-main', 'bw', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_enqueue_scripts', 'fusion_child_enqueue_styles', 2000);

/**
 * Image sizes
 */
add_image_size( 'bw-square', 560, 560, true );

/**
 * Releases metaboxes
 */
function bw_release_meta_boxes( $meta_boxes ) {
	$prefix = 'bw-';

	$meta_boxes[] = array(
		'id' => 'release-info',
		'title' => esc_html__( 'Release Information', 'bw' ),
		'post_types' => array( 'portfolio' ),
		'context' => 'side',
		'priority' => 'high',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'catalog_number',
				'type' => 'text',
				'name' => esc_html__( 'Catalog Number', 'bw' ),
			),
			array(
				'id' => $prefix . 'artist',
				'type' => 'text',
				'name' => esc_html__( 'Artist', 'bw' ),
			),
			array(
				'id' => $prefix . 'full_release_name',
				'type' => 'text',
				'name' => esc_html__( 'Full Release Name', 'bw' ),
			),
			array(
				'id' => $prefix . 'release_date',
				'type' => 'date',
				'name' => esc_html__( 'Release Date', 'bw' ),
			),
			array(
				'id' => $prefix . 'digital',
				'name' => esc_html__( 'Digital', 'bw' ),
				'type' => 'checkbox',
			),
			array(
				'id' => $prefix . 'cd',
				'name' => esc_html__( 'CD', 'bw' ),
				'type' => 'checkbox',
			),
			array(
				'id' => $prefix . 'vinyl',
				'name' => esc_html__( 'Vinyl', 'bw' ),
				'type' => 'checkbox',
			),
		),
	);

	$meta_boxes[] = array(
		'id' => 'release-external-links',
		'title' => esc_html__( 'External Links', 'bw' ),
		'post_types' => array( 'portfolio' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'soundcloud_url',
				'type' => 'url',
				'name' => esc_html__( 'SoundCloud URL', 'bw' ),
			),
			array(
				'id' => $prefix . 'spotify_url',
				'type' => 'url',
				'name' => esc_html__( 'Spotify URL', 'bw' ),
			),
			array(
				'id' => $prefix . 'itunes_url',
				'type' => 'url',
				'name' => esc_html__( 'iTunes URL', 'bw' ),
			),
			array(
				'id' => $prefix . 'youtube_url',
				'type' => 'url',
				'name' => esc_html__( 'Youtube URL', 'bw' ),
			),
			array(
				'id' => $prefix . 'deezer_url',
				'type' => 'url',
				'name' => esc_html__( 'Deezer URL', 'bw' ),
			),
			array(
				'id' => $prefix . 'amazon_url',
				'type' => 'url',
				'name' => esc_html__( 'Amazon URL', 'bw' ),
			),
			array(
				'id' => $prefix . 'beatport_url',
				'type' => 'url',
				'name' => esc_html__( 'Digital Store URL (Beatport)', 'bw' ),
			),
			array(
				'id' => $prefix . 'deejay_url',
				'type' => 'url',
				'name' => esc_html__( 'Vinyl Release URL (Deejay.de)', 'bw' ),
			),
			array(
				'id' => $prefix . 'decks_url',
				'type' => 'url',
				'name' => esc_html__( 'Vinyl Release URL (Decks.de)', 'bw' ),
			),
			array(
				'id' => $prefix . 'juno_url',
				'type' => 'url',
				'name' => esc_html__( 'Vinyl Release URL (Juno.co.uk)', 'bw' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'bw_release_meta_boxes' );



/**
 * Filter for portfolio categories
 */
function bw_filter_portfolio_archive($query) {
	if ($query->is_archive() && $query->query['post_type'] == 'portfolio') {

		$meta = array();
		if (isset($_GET['catalog_number']) && trim($_GET['catalog_number']) != '') {
			$meta[] = array(
				'key' => 'bw-catalog_number',
				'value' => sanitize_text_field($_GET['catalog_number']),
				'compare' => 'LIKE',				
			);
		}
		if (isset($_GET['artist']) && trim($_GET['artist']) != '') {
			$meta[] = array(
				'key' => 'bw-artist',
				'value' => sanitize_text_field($_GET['artist']),
				'compare' => 'LIKE',				
			);
		}
		if (isset($_GET['release_type']) && trim($_GET['release_type']) != '') {
			$meta[] = array(
				'key' => 'bw-' . sanitize_text_field($_GET['release_type']),
				'value' => '1',
			);
		}
		if (isset($_GET['release_date_start']) && trim($_GET['release_date_start']) != '') {
			$meta[] = array(
				'key' => 'bw-release_date',
				'value' => sanitize_text_field($_GET['release_date_start']),
				'compare' => '>=',
				'type' => 'DATE'
			);
		}
		if (isset($_GET['release_date_end']) && trim($_GET['release_date_end']) != '') {
			$meta[] = array(
				'key' => 'bw-release_date',
				'value' => sanitize_text_field($_GET['release_date_end']),
				'compare' => '<=',
				'type' => 'DATE'
			);
		}

		$query->set( 'orderby', 'bw-release_date' );
		$query->set( 'order', 'DESC' );
		$query->set( 'meta_query', $meta );
		if (isset($_GET['release_name'])) {
			$query->set( 's', sanitize_text_field($_GET['release_name']) );
		}
	}
}
add_action('pre_get_posts','bw_filter_portfolio_archive');

/** 
 * Relabel all portfolio labels
 */
function bw_relabel_shipping($translated) {
    $translated = str_ireplace('Portfolio Item', 'Release', $translated);
    $translated = str_ireplace('Portfolio Items', 'Releases', $translated);
    $translated = str_ireplace('Portfolio', 'Releases', $translated);
    return $translated;
}
add_filter('gettext', 'bw_relabel_shipping');
add_filter('ngettext', 'bw_relabel_shipping');


/**
 * Remove menu items for non-admin roles
 */
function bw_remove_admin_menus_for_non_admins() {
	if( !current_user_can('administrator') && !current_user_can('editor') ):
        remove_menu_page( 'edit.php?post_type=portfolio' );		
        remove_menu_page( 'post-new.php?post_type=ae_global_templates' );		
	endif;
	if( !current_user_can( 'administrator' ) ):
        remove_menu_page( 'edit.php?post_type=ae_global_templates' );
        remove_menu_page( 'edit.php?post_type=elementor_library' );
        remove_menu_page( 'wpcf7' );
        remove_menu_page( 'tools.php' );
        remove_submenu_page( 'edit.php?post_type=page', 'post-new.php?post_type=page' );
    endif;
}
add_action( 'admin_menu', 'bw_remove_admin_menus_for_non_admins', 100 );

/**
 * Remove admin bar 'new' links
 */
function bw_remove_admin_bar_new_links() 
{
	global $wp_admin_bar;   
	$wp_admin_bar->remove_node( 'new-ae_global_templates' );
	if( !current_user_can('administrator') && !current_user_can('editor') ):		
		$wp_admin_bar->remove_node( 'new-portfolio' );
		$wp_admin_bar->remove_node( 'new-page' );
	endif;
	if( !current_user_can('administrator') ):		
		$wp_admin_bar->remove_node( 'new-page' );
	endif;
}
add_action( 'admin_bar_menu', 'bw_remove_admin_bar_new_links', 999 );

/**
 * Remove new page button
 */
function bw_remove_add_new_page_button() 
{
	if( !current_user_can('administrator') ):
		?>
		<style>
			.post-type-page .page-title-action {
				display: none;
			}
			#elementor-switch-mode {
				display: none;
			}
			span.edit_with_elementor {
				display: none;
			}
		</style>
		<?php
	endif;
}
add_action( 'admin_head', 'bw_remove_add_new_page_button', 999 );

/*
 * Restrict URL access
 */
function bw_restrict_admin_pages() {
	
	$restrictions = array(
		'admin.php?page=wpcf7',
		'edit.php?post_type=ae_global_templates',
		'edit.php?post_type=elementor_library',
		'post-new.php?post_type=page',
	);

	foreach ( $restrictions as $restriction ) {
		if ( ! current_user_can( 'administrator' ) && strpos($_SERVER['REQUEST_URI'], $restriction) !== false ) {
			wp_redirect( admin_url() );
			exit;
		}
	}
	
}
add_action( 'admin_init', 'bw_restrict_admin_pages' );

/**
 * Add and remove foles
 */
add_role('super_contributor', __('Super Contributor')
	// Handle capabilities from role editor plugin
	// array(
	// 	'read' => true, // Allows a user to read
	// 	'create_posts' => true, // Allows user to create new posts
	// 	'edit_posts' => true, // Allows user to edit their own posts
	// )
);
remove_role( 'author' );
remove_role( 'subscriber' );

/**
 * Update comment via ajax
 */
function bw_update_comment() {
	$user = wp_get_current_user();
	$comment = get_comment($_POST['comment_id']);
	if ($user->user_email == $comment->comment_author_email) {
		$new_comment = array();
		$new_comment['comment_ID'] = $_POST['comment_id'];
		$new_comment['comment_content'] = $_POST['comment_content'];
		wp_update_comment( $new_comment );
		echo $new_comment['comment_ID'];
		die();
	}
}
add_action('wp_ajax_bw_update_comment', 'bw_update_comment');

/**
 * Add in edit links for own comments
 */
function bw_add_edit_comment_link($comment_text, $comment) {
	$user = wp_get_current_user();
	if ($user && $user->user_email == $comment->comment_author_email) {
		$comment_text = '<div class="comment-text">' . $comment_text . '</div>';
		$comment_text .= '<a href="#" class="edit-comment">Edit comment</a>';
		$comment_text .= '<form class="update-comment-form">';
		$comment_text .= '<input type="hidden" name="comment_id" value="' . $comment->comment_ID . '">';
		$comment_text .= '<textarea name="comment_content">' . $comment->comment_content . '</textarea>';
		$comment_text .= '<input type="submit" value="Update" class="btn btn-sm">';
		$comment_text .= '<a href="#" class="cancel-edit-comment">Cancel</a>';
		$comment_text .= '</form>';
	}
	return $comment_text;
}
add_filter('comment_text', 'bw_add_edit_comment_link', 10, 2);

/** 
 * Relabel Posts to News Posts
 */
function bw_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News Posts';
    $submenu['edit.php'][5][0] = 'News Posts';
    $submenu['edit.php'][10][0] = 'Add News Post';
    $submenu['edit.php'][16][0] = 'News Posts Tags';
}
function bw_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News Posts';
    $labels->singular_name = 'News Post';
    $labels->add_new = 'Add News Post';
    $labels->add_new_item = 'Add News Posts';
    $labels->edit_item = 'Edit News Posts';
    $labels->new_item = 'News Post';
    $labels->view_item = 'View News Post';
    $labels->search_items = 'Search News Posts';
    $labels->not_found = 'No News Posts found';
    $labels->not_found_in_trash = 'No News Posts found in Trash';
    $labels->all_items = 'All News Posts';
    $labels->menu_name = 'News Posts';
    $labels->name_admin_bar = 'News Posts';
}
 
add_action( 'admin_menu', 'bw_change_post_label' );
add_action( 'init', 'bw_change_post_object' );