<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fusion
 */

get_header(); ?>

        <h1 class="display-1">Releases</h1>

        <div class="row">

            <div class="col-md-3">
                <div class="sidebar-filters stick-on-scroll">
                    <a class="toggle-filters"><span class="lnr lnr-funnel"></span></a>
                    <form method="GET" action="#" id="portfolio-filters">
                        <div class="form-group">
                            <input type="text" name="artist" value="<?php echo isset($_GET['artist']) ? sanitize_text_field($_GET['artist']) : '' ?>">
                            <label>Artist</label>
                        </div>
                        <div class="form-group">                
                            <input type="text" name="release_name" id="release_name" value="<?php echo isset($_GET['release_name']) ? sanitize_text_field($_GET['release_name']) : '' ?>">
                            <label>Release Name</label>                                        
                        </div>
                        <div class="form-group select">                
                            <div class="select-wrapper has-value">
                                <select name="release_type" id="release_type">
                                    <option value="" <?php echo (isset($_GET['release_type']) && $_GET['release_type'] == '') ? 'selected' : '' ?>>Any</option>
                                    <option value="cd" <?php echo (isset($_GET['release_type']) && $_GET['release_type'] == 'cd') ? 'selected' : '' ?>>CD</option>
                                    <option value="digital" <?php echo (isset($_GET['release_type']) && $_GET['release_type'] == 'digital') ? 'selected' : '' ?>>Digital</option>
                                    <option value="vinyl" <?php echo (isset($_GET['release_type']) && $_GET['release_type'] == 'vinyl') ? 'selected' : '' ?>>Vinyl</option>
                                </select>
                            </div>
                            <label>Release Type</label>                                  
                        </div>
                        <div class="form-group">                
                            <input type="text" name="catalog_number" id="catalog_number" value="<?php echo isset($_GET['catalog_number']) ? sanitize_text_field($_GET['catalog_number']) : '' ?>">
                            <label>Catalog</label>                                        
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">                
                                    <input type="text" placeholder="From" class="date-input" name="release_date_start" id="release_date_start" value="<?php echo isset($_GET['release_date_start']) ? sanitize_text_field($_GET['release_date_start']) : '' ?>">
                                    <label class="top">Release Date</label>                                                        
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">                
                                    <input type="text" placeholder="To" class="date-input" name="release_date_end" id="release_date_end" value="<?php echo isset($_GET['release_date_end']) ? sanitize_text_field($_GET['release_date_end']) : '' ?>">
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php get_sidebar(); ?>
                </div>
            </div>



            <div class="col-md-9 portfolio-style-archive" id="posts">
                <?php
                if ( have_posts() ) : ?>
                    <div class="row wide-gutters">
                        <?php
                        $GLOBALS['image_size'] = 'bw-square';
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();
                        ?>

                            <div class="grid-item col-md-4 col-sm-6">
                                <?php						
                                /*
                                * Include the Post-Format-specific template for the content.
                                * If you want to override this in a child theme, then include a file
                                * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                */
                                get_template_part( 'template-parts/portfolio/grid', 'archive' );

                                ?>
                            </div>
                            <?php

                        endwhile;
                        ?>
                    </div>
                    <?php
                    fusion_index_navigation();

                else :

                    get_template_part( 'templates/content', 'none' );

                endif; ?>

            </div>

        </div>        
        
        <svg width="0" height="0" class="hidden">
          <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="compact-disc-cd-1">
            <circle data-name="layer2"
            cx="32" cy="32" r="30" transform="rotate(-45 31.977 32.055)" fill="none"
            stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></circle>
            <path data-name="layer2" fill="none" stroke="#202020" stroke-miterlimit="10"
            stroke-width="2" d="M39 28.1l19.1-10.9M32 2v22m-3 15.4l-8.1 20.5M4.6 44.2l20.1-9"
            stroke-linejoin="miter" stroke-linecap="round"></path>
            <circle data-name="layer1" cx="32" cy="32" r="8" transform="rotate(-45 31.977 32.055)"
            fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></circle>
            <circle data-name="layer1" cx="32" cy="32" r="1" fill="none"
            stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></circle>
          </symbol>
          <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="turntable-1">
            <circle data-name="layer2"
            cx="26" cy="33" r="1" fill="none" stroke="#202020" stroke-miterlimit="10"
            stroke-width="2" stroke-linejoin="miter" stroke-linecap="round"></circle>
            <circle data-name="layer2" cx="26" cy="33" r="8" fill="none"
            stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></circle>
            <circle data-name="layer2" cx="26" cy="33" r="24" fill="none"
            stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></circle>
            <rect data-name="layer1" x="54" y="9" width="8" height="12" rx="2"
            ry="2" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2"
            stroke-linejoin="miter" stroke-linecap="round"></rect>
            <path data-name="layer1" d="M42.1 39.8L40 36.4a2 2 0 0 0-2.7-.7L27 41.9a2 2 0 0 0-.7 2.7l2 3.4a2 2 0 0 0 2.7.7l10.3-6.2a2 2 0 0 0 .8-2.7zm-1-1.7l16.2-9.4c.9-.6.7-1.8.8-2.9V21"
            fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></path>
          </symbol>
          <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="ipod-player-1">
            <path data-name="layer1"
            d="M26 10a8 8 0 0 1 8-8 8 8 0 0 1 8 8v39a7 7 0 0 0 7 7 7.1 7.1 0 0 0 7-7.5V18"
            fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></path>
            <rect data-name="layer2" x="2" y="10" width="32" height="52" rx="2"
            ry="2" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2"
            stroke-linejoin="miter" stroke-linecap="round"></rect>
            <circle data-name="layer2" cx="18" cy="46" r="8" fill="none" stroke="#202020"
            stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter" stroke-linecap="round"></circle>
            <path data-name="layer2" fill="none" stroke="#202020" stroke-miterlimit="10"
            stroke-width="2" d="M2 30h32" stroke-linejoin="miter" stroke-linecap="round"></path>
            <path data-name="layer1" d="M53 31h-3v6h3a3 3 0 0 0 0-6zm6-16h3v6h-3a3 3 0 0 1 0-6z"
            fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></path>
          </symbol>
        </svg>
        <svg width="0" height="0" class="hidden">
          <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="music-playlist-1">
            <path data-name="layer2"
            fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" d="M10 16H2v34h8m44-34h8v34h-8"
            stroke-linejoin="miter" stroke-linecap="round"></path>
            <path data-name="layer1" d="M33.7 36H30a4 4 0 1 0 4 4v-1l-2-19s8 0 8 8"
            fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
            stroke-linecap="round"></path>
            <path data-name="layer1" fill="none" stroke="#202020" stroke-miterlimit="10"
            stroke-width="2" d="M10 10h44v44H10z" stroke-linejoin="miter" stroke-linecap="round"></path>
          </symbol>
        </svg>

<?php
get_footer();
