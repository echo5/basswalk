(function($){
    'use strict';

    /**
     * Portfolio filters
     */
    $(document).on('change', '#portfolio-filters :input', function() {
        var form = $(this).closest('form');
        $('#posts').addClass('loading');
        setGetParameters(form);
    });
    $(document).on('submit', '#portfolio-filters', function(e) {
        e.preventDefault();
    });

    function setGetParameters(form)
    {
        var inputs = form.find(':input');
        var url = window.location.href;
        var hash = location.hash;
        url = url.replace(hash, '');
        url = url.replace(/(\?.*)/g, '');
        url = url.replace(/(page\/\d*\/)/g, '');
        inputs.each(function() {
            if (url.indexOf("?") < 0)
                url += "?" + $(this).attr('name') + "=" + $(this).val();
            else
                url += "&" + $(this).attr('name') + "=" + $(this).val();
        });
        console.log(url);
        var request = $.ajax({
          url: url,
          type: "GET",
        //   data: {id : menuId},
          dataType: "html"
        });
        request.done(function(msg) {
          var newPosts = $(msg).find('#posts').html();
          $('#posts').html(newPosts);
          $('#posts').removeClass('loading');          
        });
    }

    /**
     * Datetime calendar
     */
    Barba.Dispatcher.on('showNewPage', function(currentStatus, oldStatus, container) {
        $(".date-input").flatpickr();
        flatpickr(".date-input", {});
    });
    $(document).on('ready', function() {
        $(".date-input").flatpickr();
        flatpickr(".date-input", {});
    });
        

    /**
     * Splash background
     */
    $(document).on('hover', '.branding-col', function() {
        if (!$(this).hasClass('active')) {
            $('.branding-col').removeClass('active');
            $(this).addClass('active');
            var targetBg = $(this).data('target-bg');
            $('.branding-bg').removeClass('active');
            $(targetBg).addClass('active');
        }
    });

    /**
     * Setup branding splash backgrounds
     */
    function setupBrandingSplash() {
        $('.branding-col').each(function(index) {
            var bg = $(this).find('.elementor-element-populated').css('background-image');
            bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
            var bgDiv = $('<div class="branding-bg" id="branding-bg-' + index + '"></div>');
            bgDiv.css('background-image', 'url(' + bg + ')');
            $(this).attr('data-target-bg', '#branding-bg-' + index);
            $(this).find('.elementor-element-populated').css('background-image', 'none');            
            $('body').append(bgDiv);
        });
    }
    $(document).on('ready', function() {
        setupBrandingSplash();
        $('#branding-bg-1').addClass('active');
        $('body').addClass('loaded');
    });

    /**
     * AJAX transitions
     */
    var transitions = {};
    transitions['Blur'] = Barba.BaseTransition.extend({
      start: function() {
        Promise
          .all([this.newContainerLoading, this.fadeOut()])
          .then(this.fadeIn.bind(this));
      },

      fadeOut: function() {
        $('body').addClass('loading');        
        
        var deferred = Barba.Utils.deferred();
          var $oldContainer = $(this.oldContainer);
          $('html')
             .velocity('stop')
             .velocity('scroll', { duration: 750, offset: 0 });

        Barba.Dispatcher.trigger('oldPageHidden', $oldContainer);
        setTimeout(function() {
            deferred.resolve();
        }, 100);

        return deferred.promise;
      },

      fadeIn: function() {
        var _this = this;
        var $el = $(this.newContainer);

        $(this.oldContainer).hide();
        $('body').removeClass('loading');
        $('body').attr('class', window.bodyClasses);
        $('body').addClass('loaded');
        setTimeout(function() {
            Barba.Dispatcher.trigger('showNewPage', $el);        
        }, 500);
        // $el.css({
        //   visibility : 'visible',
        //   opacity : 0
        // });

        // Change body class
        
        _this.done();
        $el.velocity({ blur: [0,10] }, {
            duration: 600,
            complete: function() {
            }
        });
      }
    });


    /**
	 *	Get transition from link
	 */
	Barba.Pjax.getTransition = function() {
	   var transitionObj = transitions['Blur'];
	   return transitionObj;
    };

    /**
     * Toggle filters
     */
    $(document).on('click', '.toggle-filters', function() {
        $('body').toggleClass('filters-active');
    });

    /**
     * Edit comment
     */
    $(document).on('click', '.edit-comment', function(e) {
        e.preventDefault();
        var comment = $(this).closest('.comment');
        comment.addClass('editing');
    });
    $(document).on('click', '.cancel-edit-comment', function(e) {
        e.preventDefault();
        var comment = $(this).closest('.comment');
        comment.removeClass('editing');
    });

    /**
     * Comment update forms
     */
    $(document).on('submit', '.update-comment-form', function(e) {
       e.preventDefault();
       console.log(bw.ajaxUrl);
       var formData = jQuery(this).serialize();
       formData += '&action=bw_update_comment';
       jQuery.ajax({
         type:    "POST",
         url:     bw.ajaxUrl,
         data:    formData,
         success: function(comment_id) {
            if(comment_id) {
                var comment = $('#comment-' + comment_id);
                comment.removeClass('editing');
                var newContent = comment.find('textarea').val();
                comment.find('.comment-text').html(newContent);
            }
         }
       });
       return false;
    });

})(jQuery);