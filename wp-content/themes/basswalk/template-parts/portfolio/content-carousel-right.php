<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-fluid">
		<div class="row">

            <div class="col-md-6 col-featured">
                <div class="featured">
                    <?php if ( !has_post_thumbnail() ) : ?>
                        <div class="no-album-img"><svg><use xlink:href="#music-playlist-1"></use></svg></div>
                    <?php else: ?>
                        <?php the_post_thumbnail('large'); ?>
                    <?php endif; ?>
                </div><!-- .featured -->
			</div>

			<div class="col-md-6 main">
				<div class="">
					<header class="entry-header page-header">
                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                        <?php get_template_part('template-parts/portfolio/release-types'); ?>
						<div class="categories">
							<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
                        </div>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php
							the_content();
						?>
                    </div><!-- .entry-content -->

                    <?php if ( $soundcloud_url = get_post_meta(get_the_ID(), 'bw-soundcloud_url', true) ): ?>
                        <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=<?php echo urlencode($soundcloud_url); ?>&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true"></iframe>
                    <?php endif; ?>

                    <div class="release-links">
                        <?php if ( $full_release_name = get_post_meta(get_the_ID(), 'bw-full_release_name', true) ): ?>
                            <h3 class="h4">Release Name</h3>                                                
                            <p><?php echo $full_release_name; ?></p>
                        <?php endif; ?>
                        <h3 class="h4">Catalog Number</h3>                        
                        <p><?php echo get_post_meta(get_the_ID(), 'bw-catalog_number', true); ?></p>
                        <h3 class="h4">Release Date</h3>                        
                        <p><?php echo date("M d, Y", strtotime(get_post_meta(get_the_ID(), 'bw-release_date', true))); ?></p>
                        <h3 class="h4">Listen &amp; Purchase</h3>
                        <?php if ( $spotify_url = get_post_meta(get_the_ID(), 'bw-spotify_url', true) ): ?>
                            <a href="<?php echo $spotify_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Spotify</a>
                        <?php endif; ?>
                        <?php if ( $itunes_url = get_post_meta(get_the_ID(), 'bw-itunes_url', true) ): ?>
                            <a href="<?php echo $itunes_url; ?>" target="_blank"><span class="lnr lnr-link"></span> iTunes</a>
                        <?php endif; ?>
                        <?php if ( $youtube_url = get_post_meta(get_the_ID(), 'bw-youtube_url', true) ): ?>
                            <a href="<?php echo $youtube_url; ?>" target="_blank"><span class="lnr lnr-link"></span> YouTube</a>
                        <?php endif; ?>
                        <?php if ( $deezer_url = get_post_meta(get_the_ID(), 'bw-deezer_url', true) ): ?>
                            <a href="<?php echo $deezer_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Deezer</a>
                        <?php endif; ?>
                        <?php if ( $amazon_url = get_post_meta(get_the_ID(), 'bw-amazon_url', true) ): ?>
                            <a href="<?php echo $amazon_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Amazon</a>
                        <?php endif; ?>
                        <?php if ( $beatport_url = get_post_meta(get_the_ID(), 'bw-beatport_url', true) ): ?>
                            <a href="<?php echo $beatport_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Beatport</a>
                        <?php endif; ?>
                        <?php if ( $deejay_url = get_post_meta(get_the_ID(), 'bw-deejay_url', true) ): ?>
                            <a href="<?php echo $deejay_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Deejay</a>
                        <?php endif; ?>
                        <?php if ( $decks_url = get_post_meta(get_the_ID(), 'bw-decks_url', true) ): ?>
                            <a href="<?php echo $decks_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Decks</a>
                        <?php endif; ?>
                        <?php if ( $juno_url = get_post_meta(get_the_ID(), 'bw-juno_url', true) ): ?>
                            <a href="<?php echo $juno_url; ?>" target="_blank"><span class="lnr lnr-link"></span> Juno</a>
                        <?php endif; ?>
                    </div>

					<?php get_template_part('template-parts/portfolio/details'); ?>

				</div>
			</div>

		</div>
	</div>
</article><!-- #post-## -->

<div class="info-bar">
    <div class="nav-previous">
        <?php
        $prev_post = get_next_post();
        if (!empty( $prev_post )): ?>
          <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>"><span class="article-name slide-in"><?php echo $prev_post->post_title ?></span></a>
        <?php endif ?>
    </div>
        <div class="col text-center">
            <a href="<?php echo home_url('/releases'); ?>">Back to Releases</a>
        </div>
    <?php
    $next_post = get_previous_post();
    if (!empty( $next_post )): ?>
      <div class="nav-next"><a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>"><span class="article-name slide-in"><?php echo $next_post->post_title ?></span></a></div>
    <?php endif ?>
</div>



<svg width="0" height="0" class="hidden">
  <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="compact-disc-cd-1">
    <circle data-name="layer2"
    cx="32" cy="32" r="30" transform="rotate(-45 31.977 32.055)" fill="none"
    stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></circle>
    <path data-name="layer2" fill="none" stroke="#202020" stroke-miterlimit="10"
    stroke-width="2" d="M39 28.1l19.1-10.9M32 2v22m-3 15.4l-8.1 20.5M4.6 44.2l20.1-9"
    stroke-linejoin="miter" stroke-linecap="round"></path>
    <circle data-name="layer1" cx="32" cy="32" r="8" transform="rotate(-45 31.977 32.055)"
    fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></circle>
    <circle data-name="layer1" cx="32" cy="32" r="1" fill="none"
    stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></circle>
  </symbol>
  <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="turntable-1">
    <circle data-name="layer2"
    cx="26" cy="33" r="1" fill="none" stroke="#202020" stroke-miterlimit="10"
    stroke-width="2" stroke-linejoin="miter" stroke-linecap="round"></circle>
    <circle data-name="layer2" cx="26" cy="33" r="8" fill="none"
    stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></circle>
    <circle data-name="layer2" cx="26" cy="33" r="24" fill="none"
    stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></circle>
    <rect data-name="layer1" x="54" y="9" width="8" height="12" rx="2"
    ry="2" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2"
    stroke-linejoin="miter" stroke-linecap="round"></rect>
    <path data-name="layer1" d="M42.1 39.8L40 36.4a2 2 0 0 0-2.7-.7L27 41.9a2 2 0 0 0-.7 2.7l2 3.4a2 2 0 0 0 2.7.7l10.3-6.2a2 2 0 0 0 .8-2.7zm-1-1.7l16.2-9.4c.9-.6.7-1.8.8-2.9V21"
    fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></path>
  </symbol>
  <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="ipod-player-1">
    <path data-name="layer1"
    d="M26 10a8 8 0 0 1 8-8 8 8 0 0 1 8 8v39a7 7 0 0 0 7 7 7.1 7.1 0 0 0 7-7.5V18"
    fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></path>
    <rect data-name="layer2" x="2" y="10" width="32" height="52" rx="2"
    ry="2" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2"
    stroke-linejoin="miter" stroke-linecap="round"></rect>
    <circle data-name="layer2" cx="18" cy="46" r="8" fill="none" stroke="#202020"
    stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter" stroke-linecap="round"></circle>
    <path data-name="layer2" fill="none" stroke="#202020" stroke-miterlimit="10"
    stroke-width="2" d="M2 30h32" stroke-linejoin="miter" stroke-linecap="round"></path>
    <path data-name="layer1" d="M53 31h-3v6h3a3 3 0 0 0 0-6zm6-16h3v6h-3a3 3 0 0 1 0-6z"
    fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></path>
  </symbol>
</svg>
<svg width="0" height="0" class="hidden">
  <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" id="music-playlist-1">
    <path data-name="layer2"
    fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" d="M10 16H2v34h8m44-34h8v34h-8"
    stroke-linejoin="miter" stroke-linecap="round"></path>
    <path data-name="layer1" d="M33.7 36H30a4 4 0 1 0 4 4v-1l-2-19s8 0 8 8"
    fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="miter"
    stroke-linecap="round"></path>
    <path data-name="layer1" fill="none" stroke="#202020" stroke-miterlimit="10"
    stroke-width="2" d="M10 10h44v44H10z" stroke-linejoin="miter" stroke-linecap="round"></path>
  </symbol>
</svg>
