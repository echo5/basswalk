<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured">
        <a href="<?php echo the_permalink(); ?>">
            <?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail($GLOBALS['image_size']); ?>
            <?php else: ?>
                <svg><use xlink:href="#music-playlist-1"></use></svg>
            <?php endif; ?>
		</a>
	</div><!-- .featured -->

    <div class="release-info">
        <a href="<?php echo the_permalink(); ?>">
            <?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
        </a>
        <div class="release-types">
            <?php get_template_part('template-parts/portfolio/release-types'); ?>
        </div>
    </div>


</article><!-- #post-## -->